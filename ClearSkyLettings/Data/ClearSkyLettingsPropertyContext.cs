﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ClearSkyLettings.Models;

namespace ClearSkyLettings.Data
{
    public class ClearSkyLettingsPropertyContext : DbContext
    {
        public ClearSkyLettingsPropertyContext (DbContextOptions<ClearSkyLettingsPropertyContext> options)
            : base(options)
        {
        }

        public DbSet<ClearSkyLettings.Models.Property> Property { get; set; }
    }
}
