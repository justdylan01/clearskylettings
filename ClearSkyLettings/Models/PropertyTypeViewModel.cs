﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace ClearSkyLettings.Models
{
    public class PropertyTypeViewModel
    {
        public List<Property> Properties { get; set; }
        public SelectList Types { get; set; }
        public string PropertyType { get; set; }
        public string Search { get; set; }
    }
}
