﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ClearSkyLettings.Migrations.ClearSkyLettingsEnquiry
{
    public partial class EnquiryMigration08 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Enquiry_Property_PropertyObjectID",
                table: "Enquiry");

            migrationBuilder.DropTable(
                name: "Property");

            migrationBuilder.DropIndex(
                name: "IX_Enquiry_PropertyObjectID",
                table: "Enquiry");

            migrationBuilder.DropColumn(
                name: "PropertyObjectID",
                table: "Enquiry");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "PropertyObjectID",
                table: "Enquiry",
                type: "int",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Property",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    AvailableDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Bathrooms = table.Column<int>(type: "int", nullable: false),
                    Beds = table.Column<int>(type: "int", nullable: false),
                    ListingDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Rent = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    Title = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Type = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Property", x => x.ID);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Enquiry_PropertyObjectID",
                table: "Enquiry",
                column: "PropertyObjectID");

            migrationBuilder.AddForeignKey(
                name: "FK_Enquiry_Property_PropertyObjectID",
                table: "Enquiry",
                column: "PropertyObjectID",
                principalTable: "Property",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
