﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace ClearSkyLettings.Areas.Identity.Data
{
    // Add profile data for application users by adding properties to the ClearSkyLettingsUser class
    public class ClearSkyLettingsUser : IdentityUser
    {
        [PersonalData]
        [Column(TypeName ="nvarchar(MAX)")]
        public string FirstName { get; set; }

        [PersonalData]
        [Column(TypeName ="nvarchar(MAX)")]
        public string LastName { get; set; }
    }
}
