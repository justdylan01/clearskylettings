﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ClearSkyLettings.Data;
using ClearSkyLettings.Models;

namespace ClearSkyLettings.Controllers
{
    public class EnquiriesController : Controller
    {
        private readonly ClearSkyLettingsEnquiryContext _contextEnquiry;

        public EnquiriesController(ClearSkyLettingsEnquiryContext contextEnquiry)
        {
            _contextEnquiry = contextEnquiry;
        }

        // GET: Enquiries
        public async Task<IActionResult> Index()
        {            
            return View(await _contextEnquiry.Enquiry.ToListAsync());
        }

        // GET: Enquiries/Success
        public async Task<IActionResult> Success()
        {
            return View(await _contextEnquiry.Enquiry.ToListAsync());
        }

        // GET: Enquiries/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var enquiry = await _contextEnquiry.Enquiry
                .FirstOrDefaultAsync(m => m.ID == id);
            if (enquiry == null)
            {
                return NotFound();
            }

            return View(enquiry);
        }

        // GET: Enquiries/Create
        public IActionResult Create()
        {
            return View();
        }        
        
        // POST: Enquiries/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,FirstName,LastName,Email,Subject,Message,PropertyName")] Enquiry enquiry)
        {
            if (ModelState.IsValid)
            {
                _contextEnquiry.Add(enquiry);
                await _contextEnquiry.SaveChangesAsync();
                return RedirectToAction(nameof(Success));
            }            

            return View(enquiry);
        }       
    }
}
